[green]1.32.1[normal] by Cheva
Used mod: Rudi´s Rush Hour 1.32 v2.0b
- decreased density (500 to 150) because of FPS
- AI patience and safety decreased

[green]1.32 v2.0b[normal] by Rudi
Überholen erlaubt version, und leichte änderung zu 1.32 v2b.

Rudi´s Rush Hour 1.32 v2.0b

In this version, there is plenty of traffic all day long.
Of course also Rush Hours in the morning and in the afternoon.

The AI's are actually also friendly, but nevertheless it comes from time to time to accidents. 
Normally it will be quickly resolved, but it can be also happens that you stuck. Therefore I recommend to quick save the game from time to time.

Features:
- No slow cars by rain and in the night. (thx piva)
- No stupid overtaking AI´s at national roads. (thx Todor Alin)
- More traffic in the citys, (thx piva) but not to much, i don´t like it.


Should be there any problems or bugs, then please contact me. Otherwise I can not look whether I can fix.
You can find me in the SCS Forum:
https://forum.scssoft.com/viewtopic.php?f=177&t=226730

This mod should work with any maps, and Skin/Trailer packs.

Credits: Rudi and...
piva and Todor Alin for some hints and files. Thanks a lot. 
And of course all who helped me to test this mod from the ProMods forum.
My thread in german language: https://promods.net/viewtopic.php?f=18&t=16355&sid=896c745cd060f88ec5e2df56e6395e7b


Loading order: Place it in the Modmanager higher then any other traffic/skin/trailer and map mods. 


Have fun. :)

11.08.2018
Little fix for 1.32. 
But i have not really tested the behavior in 1.32 yet.

24.08.2018
New fix for 1.32 Beta, and some changes in the AI behavior.
It´s still more a Beta version!!

20.09.2018
One more little necessary fix.